# Getting Started

Set balance for given account in PLN:

```curl --header "Content-Type: application/json" http://localhost:8080/accounts/<ACCOUNT_NUMBER> -d '{ "balance": 500.00 }'```

Get balance in EUR:

```curl http://localhost:8080/accounts/<ACCOUNT_NUMBER>```

