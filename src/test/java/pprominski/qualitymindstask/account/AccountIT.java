package pprominski.qualitymindstask.account;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

import java.math.BigDecimal;

import pprominski.qualitymindstask.account.dto.BalanceInCurrencyDto;
import pprominski.qualitymindstask.common.AccountNumber;
import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.ExchangeRateFacade;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class AccountIT {
    private static final AccountNumber ACCOUNT_NUMBER = new AccountNumber("test-account-number");
    private static final CurrencyCode EUR_CURRENCY_CODE = new CurrencyCode("EUR");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ExchangeRateFacade exchangeRateFacade;
    @Autowired
    private AccountFacade accountFacade;

    @RegisterExtension
    static WireMockExtension wireMockServer = WireMockExtension.newInstance()
            .options(wireMockConfig().port(12121))
            .build();

    @BeforeAll
    static void stubExchangeRatesClient() {
        wireMockServer.stubFor(
                WireMock.get(urlPathEqualTo("/api/exchangerates/rates/a/EUR"))
                        .willReturn(aResponse()
                                .withHeader("Content-Type", APPLICATION_JSON_VALUE)
                                .withBody(
                                        "{\n" +
                                                "  \"table\": \"A\",\n" +
                                                "  \"currency\": \"euro\",\n" +
                                                "  \"code\": \"EUR\",\n" +
                                                "  \"rates\": [\n" +
                                                "    {\n" +
                                                "      \"no\": \"046/A/NBP/2022\",\n" +
                                                "      \"effectiveDate\": \"2022-03-08\",\n" +
                                                "      \"mid\": 4.9999\n" +
                                                "    }\n" +
                                                "  ]\n" +
                                                "}"
                                )));
    }

    @Test
    void testGetAllTodosShouldReturnDataFromClient() throws Exception {
        // given
        stubExchangeRatesClient();
        exchangeRateFacade.fetchAndStoreExchangeRate(EUR_CURRENCY_CODE);
        accountFacade.saveBalance(ACCOUNT_NUMBER, BigDecimal.valueOf(500));

        // when & then
        mockMvc.perform(get("/accounts/" + ACCOUNT_NUMBER.getNumber()))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\n" +
                        "  \"currencyCode\": \"EUR\",\n" +
                        "  \"balance\": 100.0020\n" +
                        "}"
                ));
    }
}
