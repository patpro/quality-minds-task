package pprominski.qualitymindstask.account;

import pprominski.qualitymindstask.exchangerate.ExchangeRateConfiguration;
import pprominski.qualitymindstask.exchangerate.ExchangeRateFacade;
import pprominski.qualitymindstask.exchangerate.ExchangeRateRepository;
import pprominski.qualitymindstask.exchangerate.ExchangeRateClient;
import pprominski.qualitymindstask.exchangerate.InMemoryExchangeRateRepository;
import pprominski.qualitymindstask.exchangerate.InMemoryExchangeRateClient;

public class AccountUnitTestBase {
    private final ExchangeRateConfiguration exchangeRateConfiguration = new ExchangeRateConfiguration();
    private final ExchangeRateClient exchangeRateClient = new InMemoryExchangeRateClient(4.9999);
    private final ExchangeRateRepository exchangeRateRepository = new InMemoryExchangeRateRepository();
    protected final ExchangeRateFacade exchangeRateFacade = exchangeRateConfiguration.exchangeRateFacade(
            exchangeRateClient,
            exchangeRateRepository
    );
    private final AccountConfiguration accountConfiguration = new AccountConfiguration();
    private final AccountRepository accountRepository = accountConfiguration.accountRepository();
    protected final AccountFacade accountFacade = accountConfiguration.accountFacade(
            accountRepository,
            exchangeRateFacade
    );
}
