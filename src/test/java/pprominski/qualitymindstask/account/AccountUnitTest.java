package pprominski.qualitymindstask.account;

import static java.math.RoundingMode.HALF_EVEN;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.math.BigDecimal;

import pprominski.qualitymindstask.account.dto.BalanceInCurrencyDto;
import pprominski.qualitymindstask.common.AccountNumber;
import pprominski.qualitymindstask.common.CurrencyCode;

import org.junit.jupiter.api.Test;

class AccountUnitTest extends AccountUnitTestBase {
    private static final AccountNumber ACCOUNT_NUMBER = new AccountNumber("test-account-number");
    private static final CurrencyCode EUR_CURRENCY_CODE = new CurrencyCode("EUR");

    @Test
    void shouldFetchAccountBalanceInEuro() {
        // given
        exchangeRateFacade.fetchAndStoreExchangeRate(EUR_CURRENCY_CODE);
        accountFacade.saveBalance(ACCOUNT_NUMBER, BigDecimal.valueOf(500));

        // when
        BalanceInCurrencyDto balanceInEuro = accountFacade.calculateBalance(
                ACCOUNT_NUMBER,
                EUR_CURRENCY_CODE
        );

        // then
        BalanceInCurrencyDto expectedBalanceInEuro = new BalanceInCurrencyDto(
                EUR_CURRENCY_CODE.getCode(),
                BigDecimal.valueOf(100.0020).setScale(4, HALF_EVEN)
        );
        assertThat(balanceInEuro).isEqualTo(expectedBalanceInEuro);
    }
}
