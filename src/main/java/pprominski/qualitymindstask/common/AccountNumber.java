package pprominski.qualitymindstask.common;

import lombok.Value;

@Value
public class AccountNumber {
    String number;
}
