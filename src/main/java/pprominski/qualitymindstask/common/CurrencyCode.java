package pprominski.qualitymindstask.common;

import lombok.Value;

@Value
public class CurrencyCode {
    String code;
}
