package pprominski.qualitymindstask.exchangerate;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.dto.ExchangeRateDto;

import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class ExchangeRateFetcher {
    private final ExchangeRateClient exchangeRateClient;
    private final ExchangeRateRepository exchangeRateRepository;

    void fetchLatestAndStoreExchangeRate(CurrencyCode currencyCode) {
        ExchangeRateDto exchangeRateDto = exchangeRateClient.fetchLatestExchangeRate(currencyCode);
        exchangeRateRepository.save(currencyCode, BigDecimal.valueOf(exchangeRateDto.getExchangeRate()));
    }
}
