package pprominski.qualitymindstask.exchangerate;

import java.util.List;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.dto.ExchangeRateDto;
import pprominski.qualitymindstask.exchangerate.dto.ExchangeRateDto.RateDto;

@RequiredArgsConstructor
public class InMemoryExchangeRateClient implements ExchangeRateClient {
    private final Double exchangeRate;

    @Override
    public ExchangeRateDto fetchLatestExchangeRate(CurrencyCode currencyCode) {
        return new ExchangeRateDto(
                currencyCode.getCode(),
                List.of(new RateDto(exchangeRate))
        );
    }
}
