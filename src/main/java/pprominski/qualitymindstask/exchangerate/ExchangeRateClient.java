package pprominski.qualitymindstask.exchangerate;

import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.dto.ExchangeRateDto;

public interface ExchangeRateClient {
    ExchangeRateDto fetchLatestExchangeRate(CurrencyCode currencyCode);
}
