package pprominski.qualitymindstask.exchangerate;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.CurrencyCode;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;

@RequiredArgsConstructor
public class ExchangeRateFacade {
    private final ExchangeRateFetcher exchangeRateFetcher;
    private final ExchangeRateRetriever exchangeRateRetriever;

    public void fetchAndStoreExchangeRate(CurrencyCode currencyCode) {
        exchangeRateFetcher.fetchLatestAndStoreExchangeRate(currencyCode);
    }

    public BigDecimal getLatestExchangeRate(CurrencyCode currencyCode) {
        return exchangeRateRetriever.getExchangeRate(currencyCode);
    }

    @Scheduled(cron = "15 12 * * 1-5")
    @EventListener(ApplicationReadyEvent.class)
    public void fetchAndStoreEuroExchangeRate() {
        fetchAndStoreExchangeRate(new CurrencyCode("EUR"));
    }
}
