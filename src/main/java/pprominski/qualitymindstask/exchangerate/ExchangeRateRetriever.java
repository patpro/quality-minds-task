package pprominski.qualitymindstask.exchangerate;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.CurrencyCode;

@RequiredArgsConstructor
class ExchangeRateRetriever {
    private final ExchangeRateRepository repository;

    BigDecimal getExchangeRate(CurrencyCode currencyCode) {
        return repository.getByCurrencyCode(currencyCode);
    }
}
