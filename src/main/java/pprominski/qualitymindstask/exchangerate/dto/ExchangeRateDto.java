package pprominski.qualitymindstask.exchangerate.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
public class ExchangeRateDto {
    String code;
    List<RateDto> rates;

    public Double getExchangeRate() {
        return rates.get(0).getMid();
    }

    @Value
    public static class RateDto {
        Double mid;
    }
}
