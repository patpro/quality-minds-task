package pprominski.qualitymindstask.exchangerate;

import java.math.BigDecimal;

import pprominski.qualitymindstask.common.CurrencyCode;

public interface ExchangeRateRepository {
    void save(CurrencyCode currencyCode, BigDecimal exchangeRate);
    BigDecimal getByCurrencyCode(CurrencyCode currencyCode);
}
