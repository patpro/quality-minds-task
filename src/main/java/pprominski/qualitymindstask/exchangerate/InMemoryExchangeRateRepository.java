package pprominski.qualitymindstask.exchangerate;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import pprominski.qualitymindstask.common.CurrencyCode;

public class InMemoryExchangeRateRepository implements ExchangeRateRepository {
    private final Map<CurrencyCode, BigDecimal> exchangeRates = new ConcurrentHashMap<>();

    @Override
    public void save(CurrencyCode currencyCode, BigDecimal exchangeRate) {
        exchangeRates.put(currencyCode, exchangeRate);
    }

    @Override
    public BigDecimal getByCurrencyCode(CurrencyCode currencyCode) {
        return exchangeRates.get(currencyCode);
    }
}
