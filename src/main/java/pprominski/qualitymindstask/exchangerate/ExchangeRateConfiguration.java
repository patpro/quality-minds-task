package pprominski.qualitymindstask.exchangerate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExchangeRateConfiguration {
    
    @Bean
    public ExchangeRateRepository exchangeRatesRepository() {
        return new InMemoryExchangeRateRepository();
    }

    @Bean
    public ExchangeRateFacade exchangeRateFacade(
            ExchangeRateClient exchangeRateClient,
            ExchangeRateRepository exchangeRateRepository
    ) {
        ExchangeRateFetcher exchangeRateFetcher = new ExchangeRateFetcher(
                exchangeRateClient,
                exchangeRateRepository
        );
        ExchangeRateRetriever exchangeRateRetriever = new ExchangeRateRetriever(exchangeRateRepository);
        return new ExchangeRateFacade(
                exchangeRateFetcher,
                exchangeRateRetriever
        );
    }
}
