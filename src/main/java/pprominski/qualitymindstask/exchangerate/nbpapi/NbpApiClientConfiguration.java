package pprominski.qualitymindstask.exchangerate.nbpapi;

import pprominski.qualitymindstask.exchangerate.ExchangeRateClient;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(NbpApiConfigurationProperties.class)
public class NbpApiClientConfiguration {

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ExchangeRateClient exchangeRatesClient(
            RestTemplate restTemplate,
            NbpApiConfigurationProperties nbpApiConfigurationProperties
    ) {
        return new NbpApiClient(
                restTemplate,
                nbpApiConfigurationProperties
        );
    }
}
