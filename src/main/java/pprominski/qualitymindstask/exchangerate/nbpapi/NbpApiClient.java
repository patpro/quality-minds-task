package pprominski.qualitymindstask.exchangerate.nbpapi;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.ExchangeRateClient;
import pprominski.qualitymindstask.exchangerate.dto.ExchangeRateDto;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
public
class NbpApiClient implements ExchangeRateClient {
    private final RestTemplate restTemplate;
    private final NbpApiConfigurationProperties nbpApiConfigurationProperties;

    @Override
    public ExchangeRateDto fetchLatestExchangeRate(CurrencyCode currencyCode) {
        return restTemplate.getForObject(nbpApiConfigurationProperties.getBaseUrl() +
                "/api/exchangerates/rates/a/" + currencyCode.getCode(), ExchangeRateDto.class);
    }
}
