package pprominski.qualitymindstask.exchangerate.nbpapi;

import lombok.Data;
import lombok.Value;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "nbp-api")
@Data
class NbpApiConfigurationProperties {
    String baseUrl;
}
