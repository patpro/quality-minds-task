package pprominski.qualitymindstask.account;

import static java.math.RoundingMode.HALF_EVEN;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.AccountNumber;
import pprominski.qualitymindstask.common.CurrencyCode;
import pprominski.qualitymindstask.exchangerate.ExchangeRateFacade;

@RequiredArgsConstructor
class AccountInForeignCurrencyCalculator {
    private final AccountRetriever accountRetriever;
    private final ExchangeRateFacade exchangeRateFacade;

    BigDecimal calculateAccountValueInForeignCurrency(
            AccountNumber accountNumber,
            CurrencyCode currencyCode
    ) {
        BigDecimal balance = accountRetriever.getBalanceForAccount(accountNumber);
        BigDecimal exchangeRate = exchangeRateFacade.getLatestExchangeRate(currencyCode);
        return balance.divide(exchangeRate, 4, HALF_EVEN);
    }
}
