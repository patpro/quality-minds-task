package pprominski.qualitymindstask.account.http;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
class BalanceDto {
    BigDecimal balance;
}
