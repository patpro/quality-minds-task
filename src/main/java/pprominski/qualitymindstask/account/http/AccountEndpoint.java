package pprominski.qualitymindstask.account.http;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.account.AccountFacade;
import pprominski.qualitymindstask.account.dto.BalanceInCurrencyDto;
import pprominski.qualitymindstask.common.AccountNumber;
import pprominski.qualitymindstask.common.CurrencyCode;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts/{accountNumber}")
@RequiredArgsConstructor
class AccountEndpoint {
    private final AccountFacade accountFacade;

    @PostMapping
    public BalanceInCurrencyDto saveAccountWithBalance(
            @PathVariable String accountNumber,
            @RequestBody BalanceDto balanceDto
    ) {
        accountFacade.saveBalance(new AccountNumber(accountNumber), balanceDto.getBalance());
        return new BalanceInCurrencyDto("PLN", balanceDto.getBalance());
    }

    @GetMapping
    public BalanceInCurrencyDto calculateBalanceInForeignCurrency(@PathVariable String accountNumber) {
        return accountFacade.calculateBalance(new AccountNumber(accountNumber), new CurrencyCode("EUR"));
    }
}
