package pprominski.qualitymindstask.account;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.AccountNumber;

@RequiredArgsConstructor
class AccountSaver {
    private final AccountRepository accountRepository;

    void saveBalance(
            AccountNumber accountNumber,
            BigDecimal balance
    ) {
        accountRepository.save(accountNumber, balance);
    }
}
