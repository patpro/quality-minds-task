package pprominski.qualitymindstask.account;

import java.math.BigDecimal;

import pprominski.qualitymindstask.common.AccountNumber;

public interface AccountRepository {
    void save(AccountNumber number, BigDecimal balance);
    BigDecimal getBalance(AccountNumber number);
}
