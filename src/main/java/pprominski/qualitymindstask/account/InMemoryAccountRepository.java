package pprominski.qualitymindstask.account;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import pprominski.qualitymindstask.common.AccountNumber;

class InMemoryAccountRepository implements AccountRepository {
    private final Map<AccountNumber, BigDecimal> balances = new ConcurrentHashMap<>();

    @Override
    public void save(AccountNumber number, BigDecimal balance) {
        balances.put(number, balance);
    }

    @Override
    public BigDecimal getBalance(AccountNumber number) {
        return balances.get(number);
    }
}
