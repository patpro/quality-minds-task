package pprominski.qualitymindstask.account;

import pprominski.qualitymindstask.exchangerate.ExchangeRateFacade;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AccountConfiguration {

    @Bean
    public AccountRepository accountRepository() {
        return new InMemoryAccountRepository();
    }

    @Bean
    public AccountFacade accountFacade(
            AccountRepository accountRepository,
            ExchangeRateFacade exchangeRateFacade
    ) {
        AccountSaver accountSaver = new AccountSaver(accountRepository);
        AccountRetriever accountRetriever = new AccountRetriever(accountRepository);
        AccountInForeignCurrencyCalculator accountInForeignCurrencyCalculator = new AccountInForeignCurrencyCalculator(
                accountRetriever,
                exchangeRateFacade
        );
        return new AccountFacade(
                accountSaver,
                accountInForeignCurrencyCalculator
        );
    }
}
