package pprominski.qualitymindstask.account.dto;

import java.math.BigDecimal;

import lombok.Value;

@Value
public class BalanceInCurrencyDto {
    String currencyCode;
    BigDecimal balance;
}
