package pprominski.qualitymindstask.account;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.account.dto.BalanceInCurrencyDto;
import pprominski.qualitymindstask.common.AccountNumber;
import pprominski.qualitymindstask.common.CurrencyCode;

@RequiredArgsConstructor
public class AccountFacade {
    private final AccountSaver accountSaver;
    private final AccountInForeignCurrencyCalculator accountInForeignCurrencyCalculator;

    public void saveBalance(AccountNumber accountNumber, BigDecimal balance) {
        accountSaver.saveBalance(accountNumber, balance);
    }

    public BalanceInCurrencyDto calculateBalance(AccountNumber accountNumber, CurrencyCode currencyCode) {
        BigDecimal balanceInForeignCurrency = accountInForeignCurrencyCalculator.calculateAccountValueInForeignCurrency(
                accountNumber,
                currencyCode
        );
        return new BalanceInCurrencyDto(currencyCode.getCode(), balanceInForeignCurrency);
    }
}
