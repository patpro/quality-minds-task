package pprominski.qualitymindstask.account;

import java.math.BigDecimal;

import lombok.RequiredArgsConstructor;
import pprominski.qualitymindstask.common.AccountNumber;

@RequiredArgsConstructor
class AccountRetriever {
    private final AccountRepository accountRepository;

    BigDecimal getBalanceForAccount(AccountNumber number) {
        return accountRepository.getBalance(number);
    }
}
