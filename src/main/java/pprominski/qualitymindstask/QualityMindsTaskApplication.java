package pprominski.qualitymindstask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class QualityMindsTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(QualityMindsTaskApplication.class, args);
	}

}
